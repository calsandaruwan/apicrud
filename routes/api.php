<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Customer related API routes
 */

Route::post('/customer/create', 'Api\ApiCustomerController@createCustomer');
Route::get('/customers', 'Api\ApiCustomerController@getAll');
Route::get('/customer/{id}', 'Api\ApiCustomerController@get');
Route::post('/customer/edit/{id}', 'Api\ApiCustomerController@editCustomer');
Route::post('/customer/delete/{id}', 'Api\ApiCustomerController@deleteCustomer');
