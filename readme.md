# API CRUD - Customer manage through api
This is an application that can be used to manage customers through api calls
> Clone the ap
> Composer  install
> Provide the database config in .env file
> Good to go
> Added customers are visible in the web (go to base url)

# API LIST
These are the api that can be used to manage customers
> you need to provide the base url and record id
```sh
{{baseUrl}}/customers
{{baseUrl}}/customer/{{id}}
{{baseUrl}}/customer/create
{{baseUrl}}/customer/edit/{{id}}
{{baseUrl}}customer/delete/{{id}}
```

# Get all Customer
```sh
{{baseUrl}}/customer/create
```
> Create a get request
> Below is response

```sh
{
    "status": 200,
    "error": "false",
    "message": "",
    "data": [
        {
            "id": 4,
            "first_name": "Lahiru1",
            "last_name": "Sandaruwan1",
            "email": "lsblackcyber@gmail1.com",
            "address": "Wijerama, Nugegoda1",
            "created_at": "2017-06-27 04:32:51",
            "updated_at": "2017-06-27 04:32:51",
            "deleted_at": null
        },
        {
            "id": 3,
            "first_name": "Lahiru",
            "last_name": "Sandaruwan",
            "email": "lsblackcyber@gmail.com",
            "address": "Wijerama, Nugegoda",
            "created_at": "2017-06-27 04:24:05",
            "updated_at": "2017-06-27 04:24:05",
            "deleted_at": null
        }
    ]
}
```

# Get single Customers
```sh
{{baseUrl}}/customer/{{id}}
```
> create a get request
> Below is response

```sh
    "status": 200,
    "error": "false",
    "message": "",
    "data": {
        "id": 3,
        "first_name": "Lahiru",
        "last_name": "Sandaruwan",
        "email": "lsblackcyber@gmail.com",
        "address": "Wijerama, Nugegoda",
        "created_at": "2017-06-27 04:24:05",
        "updated_at": "2017-06-27 04:24:05",
        "deleted_at": null
    }
}
```

# Create Customer
```sh
{{baseUrl}}/customer/create
```
> create a post request

```sh
f_name:Lahiru
l_name:Sandaruwan
email:lsblackcyber@gmail.com
address:Wijerama, Nugegoda

```

> Below is response

```sh
{
    "status": 200,
    "error": "false",
    "message": "",
    "data": {
        "first_name": "Lahiru",
        "last_name": "Sandaruwan",
        "email": "lsblackcyber@gmail.com",
        "address": "Wijerama, Nugegoda",
        "updated_at": "2017-06-27 04:24:05",
        "created_at": "2017-06-27 04:24:05",
        "id": 3
    }
}
```

# Edit customer
```sh
{{baseUrl}}/customer/edit/{{id}}
```
> create a post request

```sh
f_name:Lahiru
l_name:Sandaruwan
email:lsblackcyber@gmail.com
address:Wijerama, Nugegoda

```

> Below is response

```sh
{
    "status": 200,
    "error": "false",
    "message": "",
    "data": {
        "id": 4,
        "first_name": "Lahiru12",
        "last_name": "Sandaruwan2",
        "email": "lsblackcyber@gmail2.com",
        "address": "Wijerama, Nugegoda2",
        "created_at": "2017-06-27 04:32:51",
        "updated_at": "2017-06-27 04:37:53",
        "deleted_at": null
    }
}
```

# Delete Customer
```sh
{{baseUrl}}/customer/delete/{{id}}
```
> create a post request
> Below is response

```sh
{
    "status": 200,
    "error": "false",
    "message": "",
    "data": {
        "id": 3,
        "first_name": "Lahiru",
        "last_name": "Sandaruwan",
        "email": "lsblackcyber@gmail.com",
        "address": "Wijerama, Nugegoda",
        "created_at": "2017-06-27 04:24:05",
        "updated_at": "2017-06-27 04:40:13",
        "deleted_at": "2017-06-27 04:40:13"
    }
}
```

