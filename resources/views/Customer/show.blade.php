<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .data {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .card {
                padding: 20px;
                border: solid #000 1px;
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Our Customers
                </div>

                <div class="links">
                    @if ($customers)
                        @foreach ($customers as $customer)
                            <div class="card">
                                <h2>{{ $customer->first_name.' '.$customer->last_name }}</h2>
                                <div class="data">Id : {{ $customer->id }}</div>
                                <div class="data">Email : {{ $customer->email }}</div>
                                <div class="data">Address : {{ $customer->address }}</div>
                            </div>    
                        @endforeach
                    @else
                        <div class="card">
                            <div class="data">No users have registered currently.</div>
                            <div class="data">Please Register Customers through API.</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
