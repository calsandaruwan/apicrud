<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use App\Interfaces\CustomerInterface;
Use Exception;

class ApiCustomerController extends BaseController {

    private $customer;

    public function __construct(CustomerInterface $customerInterface) {
        $this->customer = $customerInterface;
    }
    
    public function getAll() {
        $response = $this->customer->getAll();
        if ($response) {
            return format_response([
                'error' => 'false',
                'data' => $response
            ]);
        }
        return format_response([
            'message' => 'No record found'
        ]);
    }

    public function get($id) {
        try {
            return format_response([
                'error' => 'false',
                'data' => $this->customer->get($id)
            ]);
        } catch (Exception $e) {
            return format_response([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function createCustomer() {
        try {
            $rules = array(
                'f_name' => 'required',
                'l_name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
            );

            $messages = array(
                'email.email' => 'Invalid email',
            );

            $validator = Validator::make(\Request::all(), $rules, $messages);

            if ($validator->passes()) {

                $response = $this->customer->create();
                if ($response) {
                    return format_response([
                        'error' => 'false',
                        'data' => $response
                    ]);
                }
                return format_response([
                    'message' => "Something Went Wrong",
                    'data' => \Request::all()
                ]);
            } else {
                return format_response([
                    'message' => $validator->getMessageBag()->toArray(),
                    'data' => \Request::all()
                ]);
            }
        } catch (Exception $e) {
            return format_response([
                'message' => $e->getMessage(),
                'data' => \Request::all()
            ]);
        }
    }

    public function editCustomer($id) {
        try {
            $rules = array(
                'f_name' => 'required',
                'l_name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
            );

            $messages = array(
                'email.email' => 'Invalid email'
            );

            $validator = Validator::make(\Request::all(), $rules, $messages);

            if ($validator->passes()) {

                $response = $this->customer->edit($id);
                if ($response) {
                    return format_response([
                        'error' => 'false',
                        'data' => $response
                    ]);
                }
                return format_response([
                    'message' => "Something went wrong",
                    'data' => \Request::all()
                ]);
            } else {
                return format_response([
                    'message' => $validator->getMessageBag()->toArray(),
                    'data' => \Request::all()
                ]);
            }
        } catch (Exception $e) {
            return format_response([
                'message' => $e->getMessage(),
                'data' => \Request::all()
            ]);
        }
    }

    public function deleteCustomer($id) {
        try {
            $response = $this->customer->delete($id);
            if ($response) {
                return format_response([
                    'error' => 'false',
                    'data' => $response
                ]);
            }
            return format_response([
                'message' => "Something went wrong",
                'data' => \Request::all()
            ]);
        } catch (Exception $e) {
            return format_response([
                'message' => $e->getMessage()
            ]);
        }
    }

}
