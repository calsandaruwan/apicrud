<?php

namespace App\Http\Controllers\Web;

use Illuminate\Routing\Controller as BaseController;
use App\Interfaces\CustomerInterface;


class AdminCustomerController extends BaseController{
    
    private $customer;

    public function __construct(CustomerInterface $customerInterface) {
        $this->customer = $customerInterface;
    }
    
    public function show(){
        $customers = $this->customer->getAll();
        return \View::make('Customer/show',[
            'customers'=>$customers
        ]);
    }
}
