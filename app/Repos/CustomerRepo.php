<?php

namespace App\Repos;

use App\Interfaces\CustomerInterface;
use App\Models\Customer;

class CustomerRepo implements CustomerInterface {

    private $customer;

    //inject dependencies
    public function __construct(Customer $customer) {
        $this->customer = $customer;
    }

    //implementation of customer interface
    
    //get all customers
    public function getAll() {
        $data =  $this->customer->orderBy('updated_at', 'desc')->get();
        if(count($data)){
            return $data;
        }
        return false;
    }

    //get single customer by id
    public function get($id) {
        return $this->customer->findOrFail($id);
    }

    //edit customer by id
    public function edit($id) {
        $customer = $this->customer->findOrFail($id);
        
        $customer->first_name = \Request::get('f_name');
        $customer->last_name = \Request::get('l_name');
        $customer->email = \Request::get('email');
        $customer->address = \Request::get('address');
                
        if ($customer->save()) {
            return $customer;
        }
        return false;
        
    }

    //create new customer
    public function create() {
        $this->customer->first_name = \Request::get('f_name');
        $this->customer->last_name = \Request::get('l_name');
        $this->customer->email = \Request::get('email');
        $this->customer->address = \Request::get('address');

        if ($this->customer->save()) {
            return $this->customer;
        }
        return false;
    }

    //delete customer by id
    public function delete($id) {
        $customer = $this->customer->findOrfail($id);
        if($customer->delete()){
            return $customer;
        }
        return false;
    }

}
