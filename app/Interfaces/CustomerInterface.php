<?php

namespace App\Interfaces;

interface CustomerInterface{
    
    //define methods for customer interface
    public function getAll();
    public function get($id);
    public function create();
    public function edit($id);
    public function delete($id);

}